//inicializando express
var express = require('express');
var app = express();

//bloquea al header de contener información sobre el servidor (seguridad)
app.disable('x-powered-by');

//configura la aplicacion para usar handlebars
var handlebars = require('express-handlebars').create({defaultLayout : 'main'})
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

//permite hacer referencia al directorio public
app.use(express.static(__dirname + '/public'));

//directorio apps estatico
app.use(express.static(__dirname + '/apps'));

//bootstrap in node_modules folder
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/fonts', express.static(__dirname + '/node_modules/bootstrap/dist/fonts'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

//setea el puerto
app.set('port', process.env.PORT || 3000);

app.get('/about', function(req, res){
    res.render('about');
});

app.get('/', function(req, res){
    res.render('home');
});

app.get('/genetic-algorithm', function(req, res){
    res.render('genetic-algorithm');
});

//manejando urls no existentes
app.use(function(req, res){
    res.type('text/html');
    res.status(404);
    res.render('404');
});

//manejando errores del servidor
app.use(function(err, req, res, next){
    console.log(err.stack);
    res.status(500);
    res.render('500');
});

//inicializa la aplicación en el puerto establecido
app.listen(app.get('port'), function(){
    console.log('Express started on http://localhost:' + app.get('port') + '. Presiona Ctrl-C para terminar');
});