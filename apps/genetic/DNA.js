function newDirection() {
    
  var directions = [LEFT_ARROW, RIGHT_ARROW, UP_ARROW, DOWN_ARROW];
  return directions[Math.floor(Math.random()*directions.length)];
    
}

function DNA(num){
    
    this.genes = [];
    this.fitness = 0;
    this.mutationRate = 0.4;
    
    for (var i = 0; i < num; i++) {
        this.genes[i] = newDirection();
    }
    
    this.crossover = function(partner){
        var newdna = new DNA(num);
        var midpoint = random(floor(this.genes.length));
        for (var i = 0; i < this.genes.length; i++){
            if (i > midpoint){
                newdna.genes[i] = this.genes[i];
            }else{
                newdna.genes[i] = partner.genes[i];
            }
        }
        return newdna;
    }
    
    this.mutate = function(){
        for (var i = 0; i < this.genes.length; i++) {
            if (random(1) < mutationRate) {
                this.genes[i] = newDirection();
            }
        }
    }
}