function Population(num){

    this.population = [];
    this.matingpool = [];
    this.generation = 1;
    this.historicMaxFit = 0;
    
    for (var i = 0; i < num; i++) {
        this.population[i] = new Player();
    }
    

    this.evaluate = function(){
        var maxfit = 0;
        //get the maximum fitness of all the players
        for (var i = 0; i < num; i++) {
            if (this.population[i].fitness > maxfit){
                maxfit = this.population[i].fitness;
            }
        this.historicMaxFit = maxfit * 100;
        }
        //normalizacion
        for (var i = 0; i < num; i++) {
            this.population[i].fitness /= maxfit;
        }
        //llena la matingpool de acuerdo al fitness
        this.matingpool = [];
        for (var i = 0; i < num; i++) {
            var n = this.population[i].fitness * 100;
            for (var j = 0; j < n; j++){
                this.matingpool.push(this.population[i]);
            }
        }
    }
    
    this.selection = function(){
        var newPopulation = [];
        for (var i = 0; i < this.population.length; i++){
            var parentA = random(this.matingpool).DNA;
            var parentB = random(this.matingpool).DNA;
            var child = parentA.crossover(parentB);
            newPopulation[i] = new Player(child);
        }
        this.population = newPopulation;
        this.generation++;
    }
    
    this.getGeneration = function(){
        return this.generation;
    }
    
    this.getHistoricMaxFit = function(){
        return this.historicMaxFit;
    }
}