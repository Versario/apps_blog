var popu;
var player;
var target;
var obstacle1;
var lifespan = 200;
var done = false;
var info;
var obsWidth = 60;
var obsHeight = 10;
var container = document.getElementById("genetic-algorithm-container");

function setup(){
    
    info = createP("info").parent('genetic-algorithm-container');
    resetSketch();
}

function draw(){
    
    background(51);
    frameRate(30);
    fill("blue");
    ellipse(target.x, target.y, 20, 20);
    
    fill(255);
    rect(obstacle1.x, obstacle1.y, obsWidth, obsHeight);
    
    for (var i = 0; i < popu.population.length; i++){ 
        
        player = popu.population[i];
        player.update();
        player.show();
        player.calcFitness(target.x, target.y);
        player.crash(obstacle1.x, obstacle1.y, obsWidth, obsHeight);
        
        if (player.count == lifespan){
            done = true;
        }
    }
    
    if (done == true){
        popu.evaluate();
        popu.selection();
        done = false;
    }
    
    displayInfo();
}

function displayInfo(){
    
    var generation = popu.getGeneration();
    var historicMaxFit = popu.getHistoricMaxFit().toFixed(2);
    info.html("Generation: " + generation + " Max Fit: " + historicMaxFit);

}

function windowResized(){
    
    resetSketch();
}

function deviceTurned(){
    resetSketch();
}

function resetSketch(){
    
    canvas = createCanvas(container.offsetWidth - (container.offsetLeft + 10), windowHeight - 100).parent('genetic-algorithm-container');
    popu = new Population(60);
    target = createVector(width/2, 50);
    obstacle1 = createVector(width/2 - 30, 150);
}