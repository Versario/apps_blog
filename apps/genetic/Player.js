function Player(dna){
    
    if (dna){
        this.DNA = dna;
    }else{
        this.DNA = new DNA(lifespan);
    }
    
    this.count = 0;
    this.size = 10;
    this.x = width / 2;
    this.y = height / 2;
    this.xspeed = 0;
    this.yspeed = 0;
    this.fitness = 0;
    this.crashed = false;
    
    this.update = function(){
        
        if (!this.crashed){
            
            this.direction(this.DNA.genes[this.count]);
            
            this.x += this.xspeed;
            this.y += this.yspeed;
        }
        
        this.count++;
    }
    
    this.show = function(){

        fill('red');
        ellipse(this.x, this.y, this.size, this.size);
    }

    this.direction = function(gen){
                         
        if (gen === LEFT_ARROW) {
            this.xspeed = -20;
            this.yspeed = 0;

        } 
        if (gen === RIGHT_ARROW) {
            this.xspeed = 20;
            this.yspeed = 0;

        }
        if (gen === UP_ARROW) {
            this.xspeed = 0;
            this.yspeed = -20;

        }
        if (gen === DOWN_ARROW) {
            this.xspeed = 0;
            this.yspeed = 20;
                
        }
    }
    
    this.calcFitness = function(targetX, targetY){
        
        var d = dist(this.x, this.y, targetX, targetY);
        
        if (d < 15){
            this.crashed = true;
        }
        
        this.fitness = 1 / d;
        this.fitness = pow(this.fitness, 2); //exponential fitness
    }
    
    this.crash = function(obstacleX, obstacleY, obsWidth, obsHeight){
        if (this.x > obstacleX && this.x < obstacleX + obsWidth && this.y > obstacleY && this.y < obstacleY + obsHeight + 10){
            this.crashed = true;
            this.fitness /= 10;
        }
        
        if (this.x > width || this.x < 0){
            this.crashed = true;
        }
        
        if (this.y > height || this.y < 0){
            this.crashed = true;
        }    
    }
}